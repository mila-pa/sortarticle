package company.com;

/**
 * Created by student on 24.12.2016.
 */
public class Article implements Comparable<Article> {
    String name;
    int year;

    public Article(String name, int year) {
        this.name = name;
        this.year = year;
    }


    @Override
    public int compareTo(Article o) {

        int result = year - o.year;
        if (result != 0) {
            return result;
        }
        result = name.compareTo(o.name);
        return result;
    }

    @Override
    public String toString() {
        return "Article{" +
                "name='" + name + '\'' +
                ", year=" + year +
                '}';
    }
}
